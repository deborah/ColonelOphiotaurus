#!/bin/bash

# this will bounce the errbot daemon if something is too wrong for errbot's
# `!restart` to fix it.
# FIXME: hard coded to our configuration

# set up the virtual env to be running the right version of python
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME
export ERRBOT_HOME=$HOME/.colonelox/ColonelOphiotaurus
export DATA=$HOME/.colonelox/ColonelOphiotaurus/data
export PID=$DATA/err.pid

source /usr/local/bin/virtualenvwrapper.sh
workon colonelox

cd $ERRBOT_HOME
/usr/bin/skill -p `cat $PID`
errbot --daemon
