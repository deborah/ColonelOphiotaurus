import os
import re
import subprocess

from errbot import BotPlugin, re_botcmd
from time import time

MY_DIR = os.path.realpath(__file__).rsplit(os.path.sep, 1)[0]

# Try to import configuration from the cow_config file.  If there is
# no cow_config, or if any of its contents were left as None, set some
# likely defaults.
try:
    from colonel_ophiotaurus.cow_config import COW_ROOT_DIR
except ImportError:
    COW_ROOT_DIR = None

if COW_ROOT_DIR is None:
    COW_ROOT_DIR = os.path.join(MY_DIR, 'cows')

try:
    from colonel_ophiotaurus.cow_config import COW_TEXT_DIR
except ImportError:
    COW_TEXT_DIR = None

if COW_TEXT_DIR is None:
    COW_TEXT_DIR = os.path.join(COW_ROOT_DIR, 'texts')

try:
    from colonel_ophiotaurus.cow_config import COW_CORRALS
except ImportError:
    COW_CORRALS = None

if COW_CORRALS is None:
    COW_CORRALS = ['.', 'silly', 'alias']

try:
    from colonel_ophiotaurus.cow_config import COW_WORDLIST
except ImportError:
    COW_WORDLIST = None

if COW_WORDLIST is None:
    COW_WORDLIST = 'small.wordlist'

try:
    from colonel_ophiotaurus.cow_config import COW_OUT_DIR
except ImportError:
    COW_OUT_DIR = None

if COW_OUT_DIR is None:
    COW_OUT_DIR = os.path.join(COW_ROOT_DIR, 'out')

try:
    from colonel_ophiotaurus.cow_config import WEB_ROOT
except ImportError:
    WEB_ROOT = None

if WEB_ROOT is None:
    WEB_ROOT = 'http://example.com'


# Setup some default variables for the regexp parser
# regular expressions should be case insensitive & verbose
flags = re.I | re.X

# the regular expression for cow parsing
oxcmd = re.compile(
    r"""^(?P<on>on\ (?P<wordlist>[a-z]+),?\ )?      # wordlist
         (?P<show>show\ (?P<num>[0-9]+),?\ )?       # num of results
         (?P<to>to\ file,?\ )?                      # output to file
         (?P<args>.*)$""", flags)


class Ox(BotPlugin):
    """
    Handle the corral full of puzzle solver and utility cows.
    """
    def _call_to_cow(self, cmd, run_from_dir=None):
        """
        A helper that takes a command and returns the result of running the
        command.  If `run_from_dir` is not specified, will run from the
        parent of the cow directory.
        """
        # Lots of the cows are written to assume you're in the parent dir
        # of COW_ROOT_DIR. Be resilient to this.
        curdir = os.path.abspath(os.curdir)
        if run_from_dir is None:
            run_from_dir = os.path.split(COW_ROOT_DIR)[0]
        os.chdir(run_from_dir)

        # Use subprocess.PIPE, not shell=True, to prevent injection
        result = None
        try:
            pcow = subprocess.Popen(
                cmd,
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE,
            )
        except subprocess.CalledProcessError as e:
            result = "Something went wrong running {}: {}.".format(cmd, e)

        # Most of the cows don't return a useful error code
        pout, perr = pcow.communicate()
        if pout:
            result = pout.decode()
        elif perr:
            perr = perr.decode()
            self.log.debug('Error: ' + perr)
            short_error = perr.strip().splitlines()[-1]
            result = ("This cow is ill. Please ask a veterinarian to check the"
                      " logs.\nDetails: " + short_error)
        elif result is None:
            result = "This cow is uncommunicative."

        # Change back, just to be tidy
        os.chdir(curdir)
        return result

    def corral(self, arglist):
        """
        An override for a request to corral, that doesn't go through
        the cows.
        """
        assert arglist.startswith('corral'), (
            "Error: I ended up in my corral method without being asked!"
        )
        help_msg = (
            "Administrative tool to check or update the cows."
            "\n"
            "headcount: List the most recent commit messages for the Ox and"
            " the Cows."
            "\n"
            "yodel: Update both the cows and the ox to HEAD on master."
            "\n"
            "**Important**: Changes to the cow code will be immediately live."
            " If you commited a change to the **ox** code, *in order to make"
            " the code live*, you must enter a direct message conversation"
            " with `@colonelox` and say `!restart`."
        )

        if arglist == 'corral':
            return help_msg
        args = arglist.partition(' ')[2]
        if args in ('?', 'help'):
            return help_msg

        if args == 'headcount':
            msg_to_return = ['Headcounting myself:', '```']
            self_headcount = self._call_to_cow(
                ['git', 'log', '-n', '1'],
                run_from_dir=MY_DIR
            )
            msg_to_return.extend(self_headcount.splitlines()[:6])
            msg_to_return.extend(('```', ' ', 'Headcounting cows:', '```'))
            cow_headcount = self._call_to_cow(
                ['git', 'log', '-n', '1'],
                run_from_dir=COW_ROOT_DIR
            )
            msg_to_return.extend(cow_headcount.splitlines()[:6])
            msg_to_return.extend(('```', ' '))
            return '\n'.join(msg_to_return)

        if args == 'yodel':
            msg_to_return = []
            msg_to_return.append('jodaleihihu!')
            self._call_to_cow(
                ['git', 'fetch', 'origin', '-q'],
                run_from_dir=MY_DIR
            )
            msg_to_return.append(self._call_to_cow(
                ['git', 'reset', '--hard', 'origin/master'],
                run_from_dir=MY_DIR
            ))
            msg_to_return.extend(('', 'jodaleihi-moo!'))
            self._call_to_cow(
                ['git', 'fetch', 'origin', '-q'],
                run_from_dir=COW_ROOT_DIR
            )
            msg_to_return.append(self._call_to_cow(
                ['git', 'reset', '--hard', 'origin/master'],
                run_from_dir=COW_ROOT_DIR
            ))
            return '\n'.join(msg_to_return)

        return 'No can do. Sorry!'

    def process_cow(self, arglist):
        """
        Actually process a cow
        """
        # Override: if "corral", update this repo and the cows' repo
        if arglist.split()[0] == 'corral':
            return self.corral(arglist)

        cows = dict()
        # make sure we get all of the directories full of cows
        for corral in COW_CORRALS:
            for i in os.listdir(os.path.join(COW_ROOT_DIR, corral)):
                if i.endswith('.cow'):
                    # Key is the cow name, value is the full path
                    cows[os.path.splitext(i)[0]] = os.path.join(
                        COW_ROOT_DIR, corral, i
                    )
        self.log.debug("Cows found: {}".format(cows.keys()))

        # Look for the right cow
        action, _, args = arglist.partition(' ')
        action = re.sub(r'\W', '', action)  # strip out any markdown
        if action not in cows:
            return ("No cow named '{}' in the pasture. "
                    "Try `ox help`".format(action))

        self.log.debug("Found `{}` with args `{}`".format(action, args))
        # First item on the list is the full path to the cow,
        # followed by the args rejoined (since that's what cows expect)
        cmd = [cows[action]]
        if args:
            cmd.append(args)
        return self._call_to_cow(cmd)

    @re_botcmd(pattern=oxcmd)
    def ox(self, msg, match):
        """
        * `ox cow args`: runs the cow with those args, if available
        * `ox on wordlist cow args`: uses a different word list if one can
           be found, otherwise reverts to default.
        * `ox show <n> cow args`: shows the first <n> results
        * `ox to file cow args`: creates an output file
        """
        # only pay attention to ox commands
        if not str(msg).lower().startswith('ox'):
            return

        # Reset the wordlist every pass.
        os.environ['WORDLIST'] = os.path.join(COW_TEXT_DIR, COW_WORDLIST)

        # Process the incoming query
        if not match or not match.group('args'):
            # This message has no content.
            yield("Moo? You can find out more by asking 'Ox, help ox'")
        else:
            args = match.group('args')
            #
            # First look for `show`, `on`, `to file` arg prefixes
            #
            # `on`: Use a particular word list
            if match.group('on'):
                if not match.group('wordlist'):
                    # syntax error for `on`
                    error_msg = "`on` requires a word list and a cow."
                    yield(error_msg)
                    return
                dictfile = os.path.join(COW_TEXT_DIR, match.group('wordlist'))

                # Check that the list exists and is readable
                if os.access(dictfile, os.R_OK):
                    pass
                elif os.access(dictfile + '.wordlist', os.R_OK):
                    dictfile = dictfile + '.wordlist'
                else:
                    error_msg = (
                        "{} doesn't exist or isn't readable. "
                        "Using the standard word list.".format(dictfile)
                    )
                    # FIXME come up with method to send markdown to format
                    yield(error_msg)

                # Set the wordlist FIXME
                os.environ['WORDLIST'] = dictfile
                self.log.debug("Wordlist is {}".format(dictfile))

            # `to file`: output to a file, not to chat
            to_file = False
            if match.group('to'):
                outfile = args.split()[0] + str(time()) + '.txt'
                outpath = os.path.join(COW_OUT_DIR, outfile)
                to_file = True

            # `show`: change the number of lines output
            to_show = int(match.group('num') or 30)

            # Now run the cow itself
            result = self.process_cow(args).strip().splitlines()
            if not result:
                result = ['No results.']
            if to_file:
                try:
                    with open(outpath, 'w') as f:
                        for line in result:
                            f.write(line + "\n")
                    os.chmod(outpath, 447)
                    yield("Your output is at {}/{}".format(WEB_ROOT, outfile))
                except (FileNotFoundError, PermissionError) as e:
                    yield("Can't write to an output file: {}".format(e))
            else:
                yield '\n'.join(result[:to_show])
                if len(result) > to_show:
                    yield '... [{} more]'.format(len(result) - to_show)

        yield("Moo. :cow:")
