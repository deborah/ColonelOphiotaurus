# Ox-specific settings; copy this file to cow_config.py and change any or all
# of the following.

# COW_ROOT_DIR is a string specifying the root directory for Ox's cow
# searching; if None, defaults to a "cows" subdirectory of this
# directory.
COW_ROOT_DIR = None

# COW_TEXT_DIR is a string specifying the directory where the Ox will
# look for wordlists; if None, defaults to the "texts" subdirectory of
# COW_ROOT_DIR.
COW_TEXT_DIR = None

# COW_CORRALS is a list of strings that name the directories within
# COW_ROOT_DIR where the Ox will look for cows; if None, defaults to
# ['.', 'silly', 'alias'].
COW_CORRALS = None

# COW_WORDLIST is a string specifying the filename of the default
# wordlist that cows will use when searching wordlists; if None,
# defaults to "small.wordlist".
COW_WORDLIST = None

# COW_OUT_DIR is a string specifying the directory of the default
# location that cows and the colonel will use for output files; if None,
# defaults to a "output_files" subdirectory of COW_ROOT_DIR.
COW_OUT_DIR = None

# WEB_ROOT is a string specifying the directory of the default
# parent URL used by any output files; if None, defaults to
# http://example.com
WEB_ROOT = None
