# Copyright 2018 Deborah Kaplan
#
# This file is part of ColonelOphiotaurus
# Source code is available at <https://gitlab.com/deborah/ColonelOphiotaurus>.
#
# ColonelOphiotaurus is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#############################################################################
#                                                                           #
#  This config file is imported into `config.py`. Once you have added your  #
#  this file SHOULD NOT BE COMMITED TO A PUBLIC REPO.                       #
#  Copy the file `config_local_changeme.py` to `config_local.py`, then      #
#  modify any value labelled "FIXME".                                       #
#                                                                           #
#############################################################################

# Names need the @ in front!
BOT_ADMINS = ('@colonelox', '@FIXME', )

BOT_IDENTITY = {
    'team': 'FIXME',
    'server': 'FIXME',
    'token': 'FIXME',
    'port': 443,
    'timeout': 30,
    'cards_hook': 'incomingWebhookId'   # Needed for cards/attachments
}
