##################
ColonelOphiotaurus
##################

.. image:: https://gitlab.com/deborah/ColonelOphiotaurus/wikis/uploads/4351fdadd8eee3a9dc786e4c1f71fabe/ophi.png
    :align: right
    :alt: Ophiotaurus: part bull and part serpent
    
Credit: `Valchista ©2013 <https://www.deviantart.com/valchitsa/art/Ophiotaurus-364390399>`_

************
Introduction
************

In Greek mythology, the Ophiotaurus (Greek: Οφιόταυρος) was a creature that was part bull and part serpent.

A rewrite of the II&F Colonel Ox and Hell bots in Python.  ColonelOphiotaurus is essentially a plug-in for `Errbot <http://errbot.io/en/latest/index.html>`_, enabling that bot to use the existing set of puzzle solving cows. Cows can be written in any language which can be run on the bot's hosting server, as long as they are executable. It is the responsibility of the bot-runners to guarantee that no dangerous code can be placed in the cow directory. *Never run the bot as root or any other privileged user*!

Features
========

#. Unicode-compliant
#. Can handle left-to-right and right-to-left text.
#. Can talk to shell, Mattermost, XMPP/Jabber, IRC, Slack, or others.

Assumptions
===========

The Ox itself isn't inherently II&F-specific (or even puzzle-solver specific), but it is built around that logic and we have not tested it for general use.

* Server: the server running the chat protocol
* User: an instance of a user on the Server
* Hunt: An instance of a team playing an event (eg. a single puzzle boat team; II&F doing the mystery hunt)
* Team: A group of collaborating users. Each Hunt will be a Team.
* Channel: A single chat room on a Team. Will roughly correspond to a single Channel per puzzle per Hunt.
* ColonelOx: This bot.
* Cows: a directory of scripts, which can be written in any runnable language, with the file extension ".cow". This directory also includes a subdirectory named "texts" which includes all the word lists on which cows act. If you are running this for II&F, you can `check out the cows from Gitlab <https://gitlab.com/tahnan/colonel_ox>`_. Ask for access to that repository if you don't have it.

All participants on any Hunt will be Users on the Server. ColonelOx is also a User on the Server. She is a member of every Hunt, and is (probably, assuming there are no load problems) going to be invited to every Team.

ColonelOx will listen and respond in every Team she is in.

* ColonelOx can run as any logged in user. However, its saved state (including quotations, hell contents, etc.) is dependent on a database file on disk. If the bot is run from a different location without pointing to the previous instance of the persistence database, data *will not persist*.

Requirements
============

* Python 3
* `virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/latest/install.html>`_ isn't required, but will certainly make your life easier.


Configuring and installing
==========================

The instructions here assume a Mattermost chat backend and a standard II&F configuration. Alternate instructions will be added as needed.

Mattermost Setup
----------------

#. Login to Mattermost as the account that will be running the Ox bot.
#. From the Main Menu (visually the hamburger menu in the top left, beside the login name) choose *Account Settings*.
#. From the left-hand navigation, choose *Security*.
#. From *Security*, choose *Edit* to the right of *Personal Access Tokens*. If you don't see *Personal Access Tokens*, you don't have permission to create them, and you need to contact in administrator for your team.
#. Select *Create New Token*.
#. Give your token a meaningful description, such as "Bot code for Colonel Ophiotaurus".
#. Copy and paste your token; you will need it when you get to the config section below.


Installing
----------

#. Pick the user who'll run ColonelOphiotaurus.  We suggest running this in a virtualenv.
#. Make a parent directory for the config and state information.
#. Get this repository into that new directory.
#. Get the mattermost plugin repo into the ColonelOphiotaurus directory.
#. Install the pre-requsites.
#. Copy the ColonelOphiotaurus ``config_local_changeme.py`` to ``config_local.py``.
#. Copy the ColonelOphiotaurus ``config.py`` to ``config.py``.
#. Modify every instance of ``FIXME`` in ``config_local.py``.
#. Read over the settings in config.py to make sure they're right.
#. Then you need to put the cows where the Ox can find them. You can do this either by symlinking the ``cows`` directory to the ``colonel_ophiotaurus/`` directory, or by copying ``colonel_ophiotaurus/cow_config_changeme.py`` to ``colonel_ophiotaurus/cow_config.py`` and editing any necessary values.

.. code:: bash

   mkvirtualenv -p python3 colonelox        # make a virtual environment set up for Python 3
   mkdir ~/.colonelox
   cd ~/.colonelox
   git clone git@gitlab.com:deborah/ColonelOphiotaurus.git
   cd ColonelOphiotaurus
   git clone git@github.com:deborahgu/errbot-mattermost-backend.git
   pip install -r requirements.txt
   pip install -r errbot-mattermost-backend/requirements.txt
   cp config_local_changeme.py config_local.py
   vim config_local.py

   # And when that's done:
   vim config.py

   # And then symlink the cows
   ln -s /path/to/cowdir colonel_ophiotaurus/cows

You can start the bot as a daemon by running ``errbot --daemon``, though you
may want to leave it in non-daemon mode until it's tested and working.

From within the chat process (eg. Mattermost), direct message the Ox user
``!restart`` to restart the dameon and load fresh code.

Colonel Ox setup
----------------

In the colonel_ophiotaurus directory there is a cow_config_changeme.py file (analogous to the config_local_changeme.py file in the main directory that controls the errbot's configuration).  This file can be copied to cow_config.py (a file that, like config_local.py, won't be tracked), and any or all of the constants can be changed.  If cow_config.py is missing, or if any of its contents are left as `None`, the oxbot will fill in reasonable defaults, or at least defaults it thinks are reasonable.  (It's an Ox.  What it considers reasonable may not be what you consider reasonable.)

Troubleshooting
--------------

#. You can run errbot at the shell by running ``errbot -T`` from the directory containing the config files.  `Read more about this mode <http://errbot.io/en/latest/user_guide/plugin_development/development_environment.html#local-test-mode>`_.
#. You can restart the errbot from Mattermost by saying ``!restart`` in a private chat with @ColonelOx
#. If it's more hosed than that can cope with, you can say ``iifmgr, restart ox``. This should run ``bounce_ox.sh`` from the bot's server.
