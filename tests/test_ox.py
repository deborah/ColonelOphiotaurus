import logging
import os
import pytest
import subprocess
from unittest.mock import patch

# from colonel_ophiotaurus.ox import Ox

my_dir = os.path.realpath(__file__).rsplit(os.path.sep, 2)[0]
pytest_plugins = ["errbot.backends.test"]
extra_plugin_dir = os.path.join(my_dir, 'colonel_ophiotaurus/')
loglevel = logging.WARN


class MockPopen:
    @staticmethod
    def communicate():
        return b'Communicated', b''


@pytest.fixture(autouse=True)
def cow_list():
    with patch('os.listdir', return_value=['bessie.ram', 'elsie.cow']):
        yield


def talk_to_ox(testbot, message):
    plugin = testbot._bot.plugin_manager.get_plugin_obj_by_name('ox')
    return list(plugin.process_cow(message))


def test_process_cow_missing_cow(testbot):
    """
    On a cow that doesn't exist, return an error.
    """
    bessie = ''.join(talk_to_ox(testbot, 'bessie'))
    assert bessie == "No cow named 'bessie' in the pasture. Try `ox help`"


@patch('subprocess.Popen', return_value=MockPopen)
def test_process_cow_real_cow(mock_popen, testbot):
    """
    If the cows directory includes a cow file, attempt to call it.
    """
    anagram = talk_to_ox(testbot, 'elsie arguments')
    mock_popen.assert_called
    mock_popen.assert_called_with(
        [os.path.join(extra_plugin_dir, 'cows/alias/elsie.cow'), 'arguments'],
        stderr=subprocess.PIPE, stdout=subprocess.PIPE
    )
    assert ''.join(anagram) == 'Communicated'
