# Copyright 2018 Deborah Kaplan
#
# This file is part of ColonelOphiotaurus
# Source code is available at <https://gitlab.com/deborah/ColonelOphiotaurus>.
#
# ColonelOphiotaurus is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from setuptools import setup


here = os.path.abspath(os.path.dirname(__file__))


def requirements():
    """Returns requirements.txt as a list usable by setuptools"""
    here = os.path.abspath(os.path.dirname(__file__))
    reqtxt = os.path.join(here, u'requirements.txt')
    with open(reqtxt) as f:
        return f.read().split()


setup(
    name="ColonelOphiotaurus",
    version='0.1',
    description=(
        'The II&F Colonel Ox and Hell bots in Python, separating out the chat '
        'protocol modules from the chat modules.'
    ),
    url='https://gitlab.com/deborah/ColonelOphiotaurus',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",  # noqa: E501
        "Development Status :: 4 - Beta",
    ],
    keywords='bot chatbot',
    author='Deborah Kaplan',
    packages=['colonel_ophiotaurus'],
    license='AGPLv3+',
    zip_safe=False,
    install_requires=requirements(),
)
